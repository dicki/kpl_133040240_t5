/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul5.always_invoke_wait_and_await_methods_inside_a_loop;

/**
 *
 * @author dicki
 */
public class latihan {
    // Noncompliant code
    synchronized (object) {
        if (<condition does not hold>) {
            object.wait();
        }
        // Proceed when condition holds
    }
    
    // Compliant code
    synchronized (object) {
        while (<condition does not hold>) {
            object.wait();
        }
        // Proceed when condition holds
    }
}
