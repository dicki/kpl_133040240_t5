/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul5.ensure_that_tasks_submitted_to_a_thread_pool_are_interruptable;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *
 * @author dicki
 */
public final class PoolService {

    private final ExecutorService pool;

    public PoolService(int poolSize) {
        pool = Executors.newFixedThreadPool(poolSize);
    }

    public void doSomething() throws InterruptedException, IOException {
        pool.submit(new SocketReader("somehost", 8080));
        // ...
        List<Runnable> awaitingTasks = pool.shutdownNow();
    }

    public static void main(String[] args)
            throws InterruptedException, IOException {
        PoolService service = new PoolService(5);
        service.doSomething();
    }
}
